/* CS2150Coursework.java
 * A simple scene consisting of trees, stones and grasses. The scene changes its colours based on user inputs.
 * 
 * KEY_S              --->Summer Colours
 * KEY_ A            --->Autumn Colours
 * KEY_W            --->Winter Colours
 * KEY_D             --->Spring Colours
 * 
 * 26/12/2019
 * 
 * Scene Graph:
 *  Scene origin
 *  |
 *  +-- [S(20,1,20) T(0,-1,-10)] Ground plane {from lab 5}
 *  |
 *  +-- [S(20,1,10) Rx(90) T(0,4,-20)] Sky plane {from lab 5}
 *  |
 *  +-- [T(-1.45f, -0.8f, -3.6f) S(0.05f,0.06f,0.05f) R(-10f,0.0f,1.0f)] draw tree one
 *  |		|
 *  |		+--[T(0.0f, 3.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 6.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 9.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 12.0f, 0.0f)] treeBase  
 *  |		|
 *  |		+--[T(0.0f, 12.0f, 0.0f)] treeTop 
 *  |		|
 *  |		+--[T0.0f, 3.0f, 0.0f) S(1f, 0.5f, 1f)] treeTronk
 *  |
 *  |
  *  +-- [T(-1.0f, -0.8f, -3.0f) S(0.03f, 0.03f, 0.03f) R(115, 0, 1, 0)] draw tree two
 *  |		|
 *  |		+--[T(0.0f, 3.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 6.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 9.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 12.0f, 0.0f)] treeBase  
 *  |		|
 *  |		+--[T(0.0f, 12.0f, 0.0f)] treeTop 
 *  |		|
 *  |		+--[T0.0f, 3.0f, 0.0f) S(1f, 0.5f, 1f)] treeTronk
 *  |
 *  |
 *  +-- [T(0.7f, -0.8f, -5.5f) S(0.04f, 0.04f, 0.04f) R(-35, 0, 1, 0)] draw tree three
 *  |		|
 *  |		+--[T(0.0f, 3.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 6.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 9.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 12.0f, 0.0f)] treeBase  
 *  |		|
 *  |		+--[T(0.0f, 12.0f, 0.0f)] treeTop 
 *  |		|
 *  |		+--[T0.0f, 3.0f, 0.0f) S(1f, 0.5f, 1f)] treeTronk
 *  |
 *  |
 *  +-- [T(-0.6f, -0.8f, -5.0f) S(0.06f, 0.09f, 0.06f) R(100, 0, 1, 0)] draw tree four
 *  |		|
 *  |		+--[T(0.0f, 3.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 6.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 9.0f, 0.0f)] treeBase
 *  |		|
 *  |		+--[T(0.0f, 12.0f, 0.0f)] treeBase  
 *  |		|
 *  |		+--[T(0.0f, 12.0f, 0.0f)] treeTop 
 *  |		|
 *  |		+--[T0.0f, 3.0f, 0.0f) S(1f, 0.5f, 1f)] treeTronk
 *  |
 *  |
 *  +-- [T(-1.4f, -0.4f, -5.2f) S(0.2f, 0.2f, 0.2f) R(45, 0, 1, 0)] draw rock one
 *  |		|
 *  |		+--[] theRock
 *  |		|
 *  |		+--[T(3f, 6f, 5f) S(0.5f, 0.5f, 0.5f) R(30, 0, 1, 0)] theRock
 *  |
 *  |
 *  +-- [T(-0.1f, -1.0f, -4.8f) S(0.2f, 0.1f, 0.1f) R(215, 0, 1, 0] draw rock two
 *  |		|
 *  |		+--[] theRock
 *  |
 *  |
 *  +-- [T(1.3f, -0.7f, -4.6f) S(0.15f, 0.15f, 0.15f) R(-60, 0, 1, 0)] draw rock three
 *  |		|
 *  |		+--[] theRock
 *  |		|
 *  |		+--[T(3f, 6f, 5f) S(0.5f, 0.5f, 0.5f) R(30, 0, 1, 0)] theRock
 *  |
 *  |
 *  +-- [T(1.5f, 0.1f, -4.0f) S(0.05f,0.06f,0.05f) R(5, 0, 1, 0)] draw tree five {type two}
 *  |		|
 *  |		+--[] treeTypeTwo
 *  |		|
 *  |		+--[T(0.3f, -8f, 0.2f) S(0.8f, 1.0f, 0.8f)] treeTronk
 *  |		
 *  |
 *  +-- [T(0.4f, -0.5f, -3.5f) S(0.02f, 0.02f, 0.02f) R(75, 0, 1, 0)] draw tree six {type two}
 *  |		|
 *  |		+--[] treeTypeTwo
 *  |		|
 *  |		+--[T(0.3f, -8f, 0.2f) S(0.8f, 1.0f, 0.8f)] treeTronk
 *  |		
 *  |
 *  +-- [T(-0.95f, -0.6f, -2.5f) S(0.015f, 0.015f, 0.015f) R(125, 0, 1, 0)] draw tree seven {type two}
 *  |		|
 *  |		+--[] treeTypeTwo
 *  |		|
 *  |		+--[T(0.3f, -8f, 0.2f) S(0.8f, 1.0f, 0.8f)] treeTronk
 *  |		
 *  |
 *  +-- [T(-1.3f, -0.8f, -3f) S(0.010f, 0.009f, 0.010f) R(65, 0, 1, 0)] draw grass one
 *  |		|
 *  |		+--[] grass
 *  |		|
 *  |		+--[R(130, 0, 1, 0)] grass
 *  |		|
 *  |		+--[R(195, 0, 1, 0)] grass
 *  |		|
 *  |		+--[R(260, 0, 1, 0)] grass
 *  |
 *  |
 *  +-- [T(-0.75f, -0.8f, -3f) S(0.010f, 0.009f, 0.010f) R(65, 0, 1, 0)] draw grass two
 *  |		|
 *  |		+--[] grass
 *  |		|
 *  |		+--[R(130, 0, 1, 0)] grass
 *  |		|
 *  |		+--[R(195, 0, 1, 0)] grass
 *  |		|
 *  |		+--[R(260, 0, 1, 0)] grass
 *  |
 *  |
 *  +-- [T(0.55f, -0.8f, -3.40f) S(0.010f, 0.009f, 0.010f) R(65, 0, 1, 0)] draw grass three
 *  |		|
 *  |		+--[] grass
 *  |		|
 *  |		+--[R(130, 0, 1, 0)] grass
 *  |		|
 *  |		+--[R(195, 0, 1, 0)] grass
 *  |		|
 *  |		+--[R(260, 0, 1, 0)] grass
 *  |
 *  |
 *  +-- [T(1.90f, -0.8f, -4.0f) S(0.015f, 0.015f, 0.015f) R(65, 0, 1, 0)] draw grass four
 *  |		|
 *  |		+--[] grass
 *  |		|
 *  |		+--[R(130, 0, 1, 0)] grass
 *  |		|
 *  |		+--[R(195, 0, 1, 0)] grass
 *  |		|
 *  |		+--[R(260, 0, 1, 0)] grass
 *  |
 *  |
  *  */
package CS2150Coursework.Uddin;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

import GraphicsLab.Colour;
import GraphicsLab.GraphicsLab;
import GraphicsLab.Normal;
import GraphicsLab.Vertex;

/**
 * This sample demonstrates the use of user input and various types of animation
 * 
 * <p>
 * Controls:
 * <ul>
 * <li>Press A key to view Autumn Colours
 * <li>Press W key to view Winter Colours
 * <li>Press D key to view Spring Colours
 * <li>Press S key to view Summer Colours
 * </ul>
 *
 * @author Aziz Uddin 180128480
 */
public class CS2150Coursework extends GraphicsLab {

	/** display list id for scene */

	// plane Z and Y ID
	private final int planeList = 3;
	// tree base ID
	private final int treeBase = 4;
	// tree top ID
	private final int treeTop = 5;
	// tree type two ID
	private final int treeTypeTwo = 8;
	// tree trunk ID
	private final int treeTronk = 6;
	// rock ID
	private final int theRock = 7;
	// grass ID
	private final int grass = 9;
	// rotation Angle
	private float treeRotationAngle = 35.0f;

	/** ID for textures */
	private Texture groundTextures; // Y texture
	private Texture backTextures;// Z texture

	public static void main(String args[]) {
		new CS2150Coursework().run(WINDOWED, "CS2150Coursework", 0.1f);
	}

	protected void initScene() throws Exception {
		// load the textures
		groundTextures = loadTexture("CS2150Coursework/Uddin/textures/WhiteBackground.bmp");
		backTextures = loadTexture("CS2150Coursework/Uddin/textures/WhiteBackground.bmp");

		// tree base polygon
		GL11.glNewList(treeBase, GL11.GL_COMPILE);
		{
			drawTreeBase(new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0),
					new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0));
		}
		GL11.glEndList();
		// tree top polygon
		GL11.glNewList(treeTop, GL11.GL_COMPILE);
		{
			drawTreeTop(new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(25, 51, 0),
					new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0));
		}
		GL11.glEndList();
		// tree top polygon
		GL11.glNewList(treeTronk, GL11.GL_COMPILE);
		{
			drawTreeTronk(new Colour(51, 25, 0), new Colour(51, 25, 0), new Colour(51, 25, 0), new Colour(51, 25, 0),
					new Colour(51, 25, 0), new Colour(51, 25, 0));
		}
		GL11.glEndList();
		// rock polygon --> Truncated Octahedron
		GL11.glNewList(theRock, GL11.GL_COMPILE);
		{
			drawTheRock(new Colour(54, 64, 64), new Colour(96, 96, 96), new Colour(54, 64, 64), new Colour(96, 96, 96),
					new Colour(54, 64, 64), new Colour(96, 96, 96), new Colour(54, 64, 64), new Colour(96, 96, 96),
					new Colour(54, 64, 64), new Colour(54, 64, 64), new Colour(54, 64, 64), new Colour(54, 64, 64),
					new Colour(54, 64, 64), new Colour(54, 64, 64));
		}
		GL11.glEndList();
		// tree type two polygon
		GL11.glNewList(treeTypeTwo, GL11.GL_COMPILE);
		{
			drawTreeTypeTwo(new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(25, 51, 0),
					new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(0, 51, 25),
					new Colour(0, 51, 25));
		}
		GL11.glEndList();
		// grass polygon
		GL11.glNewList(grass, GL11.GL_COMPILE);
		{
			drawGrass(new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0),
					new Colour(25, 51, 0));
		}
		GL11.glEndList();
		// Z & Y plane
		GL11.glNewList(planeList, GL11.GL_COMPILE);
		{
			drawUnitPlane();
		}
		GL11.glEndList();

	}

	protected void checkSceneInput() {
		// Default Colour Summer
		if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
			// tree base polygon
			GL11.glNewList(treeBase, GL11.GL_COMPILE);
			{
				drawTreeBase(new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(25, 51, 0),
						new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(25, 51, 0),
						new Colour(51, 102, 0));
			}
			GL11.glEndList();
			// tree top polygon
			GL11.glNewList(treeTop, GL11.GL_COMPILE);
			{
				drawTreeTop(new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0),
						new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0));
			}
			GL11.glEndList();
			// rock polygon --> Truncated Octahedron
			GL11.glNewList(theRock, GL11.GL_COMPILE);
			{
				drawTheRock(new Colour(54, 64, 64), new Colour(96, 96, 96), new Colour(54, 64, 64),
						new Colour(96, 96, 96), new Colour(54, 64, 64), new Colour(96, 96, 96), new Colour(54, 64, 64),
						new Colour(96, 96, 96), new Colour(54, 64, 64), new Colour(54, 64, 64), new Colour(54, 64, 64),
						new Colour(54, 64, 64), new Colour(54, 64, 64), new Colour(54, 64, 64));
			}
			GL11.glEndList();
			// tree type two polygon
			GL11.glNewList(treeTypeTwo, GL11.GL_COMPILE);
			{
				drawTreeTypeTwo(new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(25, 51, 0),
						new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(0, 51, 25),
						new Colour(0, 51, 25));
			}
			GL11.glEndList();
			// grass polygon
			GL11.glNewList(grass, GL11.GL_COMPILE);
			{
				drawGrass(new Colour(25, 51, 0), new Colour(51, 102, 0), new Colour(25, 51, 0), new Colour(51, 102, 0),
						new Colour(25, 51, 0));
			}
			GL11.glEndList();
		}

		// Autumn Colours
		if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
			GL11.glNewList(treeBase, GL11.GL_COMPILE);
			{
				drawTreeBase(new Colour(150, 64, 0), new Colour(218, 165, 32), new Colour(150, 64, 0),
						new Colour(218, 165, 32), new Colour(150, 64, 0), new Colour(218, 165, 32),
						new Colour(150, 64, 0), new Colour(218, 165, 32));
			}
			GL11.glEndList();
			GL11.glNewList(treeTop, GL11.GL_COMPILE);
			{
				drawTreeTop(new Colour(218, 165, 32), new Colour(150, 64, 0), new Colour(218, 165, 32),
						new Colour(150, 64, 0), new Colour(218, 165, 32), new Colour(150, 64, 0),
						new Colour(218, 165, 32));
			}
			GL11.glEndList();

			GL11.glNewList(theRock, GL11.GL_COMPILE);
			{
				drawTheRock(new Colour(169, 169, 169), new Colour(192, 192, 192), new Colour(169, 169, 169),
						new Colour(192, 192, 192), new Colour(169, 169, 169), new Colour(192, 192, 192),
						new Colour(169, 169, 169), new Colour(192, 192, 192), new Colour(96, 96, 96),
						new Colour(96, 96, 96), new Colour(96, 96, 96), new Colour(96, 96, 96), new Colour(96, 96, 96),
						new Colour(96, 96, 96));
			}
			GL11.glEndList();

			GL11.glNewList(treeTypeTwo, GL11.GL_COMPILE);
			{
				drawTreeTypeTwo(new Colour(150, 64, 0), new Colour(218, 165, 32), new Colour(150, 64, 0),
						new Colour(218, 165, 32), new Colour(150, 64, 0), new Colour(218, 165, 32),
						new Colour(150, 64, 0), new Colour(150, 64, 0));
			}
			GL11.glEndList();

			GL11.glNewList(grass, GL11.GL_COMPILE);
			{
				drawGrass(new Colour(150, 64, 0), new Colour(218, 165, 32), new Colour(150, 64, 0),
						new Colour(218, 165, 32), new Colour(150, 64, 0));
			}
			GL11.glEndList();
		}

		// Winter Colours
		if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
			GL11.glNewList(treeBase, GL11.GL_COMPILE);
			{
				drawTreeBase(new Colour(173, 216, 230), new Colour(240, 248, 255), new Colour(173, 216, 230),
						new Colour(240, 248, 255), new Colour(173, 216, 230), new Colour(240, 248, 255),
						new Colour(173, 216, 230), new Colour(240, 248, 255));
			}
			GL11.glEndList();

			GL11.glNewList(treeTop, GL11.GL_COMPILE);
			{
				drawTreeTop(new Colour(240, 248, 255), new Colour(173, 216, 230), new Colour(240, 248, 255),
						new Colour(173, 216, 230), new Colour(240, 248, 255), new Colour(173, 216, 230),
						new Colour(240, 248, 255));
			}
			GL11.glEndList();

			GL11.glNewList(theRock, GL11.GL_COMPILE);
			{
				drawTheRock(new Colour(211, 211, 211), new Colour(220, 220, 220), new Colour(211, 211, 211),
						new Colour(220, 220, 220), new Colour(211, 211, 211), new Colour(220, 220, 220),
						new Colour(211, 211, 211), new Colour(220, 220, 220), new Colour(192, 192, 192),
						new Colour(192, 192, 192), new Colour(192, 192, 192), new Colour(192, 192, 192),
						new Colour(192, 192, 192), new Colour(192, 192, 192));
			}
			GL11.glEndList();

			GL11.glNewList(treeTypeTwo, GL11.GL_COMPILE);
			{
				drawTreeTypeTwo(new Colour(173, 216, 230), new Colour(240, 248, 255), new Colour(173, 216, 230),
						new Colour(240, 248, 255), new Colour(173, 216, 230), new Colour(240, 248, 255),
						new Colour(240, 248, 255), new Colour(240, 248, 255));
			}
			GL11.glEndList();

			GL11.glNewList(grass, GL11.GL_COMPILE);
			{
				drawGrass(new Colour(173, 216, 230), new Colour(240, 248, 255), new Colour(173, 216, 230),
						new Colour(240, 248, 255), new Colour(173, 216, 230));
			}
			GL11.glEndList();
		}

		// Spring Colours
		if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
			GL11.glNewList(treeBase, GL11.GL_COMPILE);
			{
				drawTreeBase(new Colour(85, 107, 47), new Colour(107, 142, 35), new Colour(85, 107, 47),
						new Colour(107, 142, 35), new Colour(85, 107, 47), new Colour(107, 142, 35),
						new Colour(85, 107, 47), new Colour(107, 142, 35));
			}
			GL11.glEndList();

			GL11.glNewList(treeTop, GL11.GL_COMPILE);
			{
				drawTreeTop(new Colour(107, 142, 35), new Colour(85, 107, 47), new Colour(107, 142, 35),
						new Colour(85, 107, 47), new Colour(107, 142, 35), new Colour(85, 107, 47),
						new Colour(107, 142, 35));
			}
			GL11.glEndList();

			GL11.glNewList(theRock, GL11.GL_COMPILE);
			{
				drawTheRock(new Colour(136, 136, 136), new Colour(160, 160, 160), new Colour(136, 136, 136),
						new Colour(160, 160, 160), new Colour(136, 136, 136), new Colour(160, 160, 160),
						new Colour(136, 136, 136), new Colour(160, 160, 160), new Colour(54, 64, 64),
						new Colour(54, 64, 64), new Colour(105, 105, 105), new Colour(105, 105, 105),
						new Colour(105, 105, 105), new Colour(105, 105, 105));
			}
			GL11.glEndList();

			GL11.glNewList(treeTypeTwo, GL11.GL_COMPILE);
			{
				drawTreeTypeTwo(new Colour(85, 107, 47), new Colour(107, 142, 35), new Colour(85, 107, 47),
						new Colour(107, 142, 35), new Colour(85, 107, 47), new Colour(107, 142, 35),
						new Colour(85, 107, 47), new Colour(85, 107, 47));
			}
			GL11.glEndList();

			GL11.glNewList(grass, GL11.GL_COMPILE);
			{
				drawGrass(new Colour(85, 107, 47), new Colour(107, 142, 35), new Colour(85, 107, 47),
						new Colour(107, 142, 35), new Colour(85, 107, 47));
			}
			GL11.glEndList();
		}

	}

	protected void updateScene() {// empty
	}

	protected void renderScene() {

		// draw the ground plane
		GL11.glPushMatrix();
		{
			// disable lighting calculations so that they don't affect
			// the appearance of the texture
			GL11.glPushAttrib(GL11.GL_LIGHTING_BIT);
			GL11.glDisable(GL11.GL_LIGHTING);
			// change the geometry colour to white so that the texture
			// is bright and details can be seen clearly
			Colour.WHITE.submit();
			// enable texturing and bind an appropriate texture
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, groundTextures.getTextureID());

			// position, scale and draw the ground plane using its display list
			GL11.glTranslatef(0.0f, -1.0f, -16.0f);
			GL11.glScalef(30.0f, 1.0f, 35.0f);
			GL11.glCallList(planeList);

			// disable textures and reset any local lighting changes
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glPopAttrib();
		}
		GL11.glPopMatrix();

		// draw the back plane
		GL11.glPushMatrix();
		{
			// disable lighting calculations so that they don't affect
			// the appearance of the texture
			GL11.glPushAttrib(GL11.GL_LIGHTING_BIT);
			GL11.glDisable(GL11.GL_LIGHTING);
			// change the geometry colour to white so that the texture
			// is bright and details can be seen clearly
			Colour.WHITE.submit();
			// enable texturing and bind an appropriate texture
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, backTextures.getTextureID());

			// position, scale and draw the back plane using its display list
			GL11.glTranslatef(0.0f, 4.0f, -10.0f);
			GL11.glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			GL11.glScalef(30.0f, 4.0f, 40.0f);
			GL11.glCallList(planeList);

			// disable textures and reset any local lighting changes
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glPopAttrib();
		}
		GL11.glPopMatrix();

		// draw tree one
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(-1.45f, -0.8f, -3.6f);
			GL11.glScalef(0.05f, 0.06f, 0.05f);
			GL11.glRotated(-10, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 3.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 6.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 9.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 12.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 12.0f, 0.0f);
				GL11.glCallList(treeTop);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 3.0f, 0.0f);
				GL11.glScalef(1f, 0.5f, 1f);

				GL11.glCallList(treeTronk);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

		// draw two three
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(-1.0f, -0.8f, -3.0f);
			GL11.glScalef(0.03f, 0.03f, 0.03f);
			GL11.glRotated(115, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 3.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 6.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 9.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 12.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 12.0f, 0.0f);
				GL11.glCallList(treeTop);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 3.0f, 0.0f);
				GL11.glScalef(1f, 0.5f, 1f);

				GL11.glCallList(treeTronk);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

		// draw tree three
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(0.7f, -0.8f, -5.5f);
			GL11.glScalef(0.04f, 0.04f, 0.04f);
			GL11.glRotated(-35, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 3.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 6.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 9.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 12.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 12.0f, 0.0f);
				GL11.glCallList(treeTop);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 3.0f, 0.0f);
				GL11.glScalef(1f, 0.5f, 1f);

				GL11.glCallList(treeTronk);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

		// draw tree four
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(-0.6f, -0.8f, -5.0f);
			GL11.glScalef(0.06f, 0.09f, 0.06f);
			GL11.glRotated(100, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 3.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 6.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 9.0f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 11.5f, 0.0f);
				GL11.glCallList(treeBase);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 11.5f, 0.0f);
				GL11.glCallList(treeTop);
			}
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0f, 3.0f, 0.0f);
				GL11.glScalef(1f, 0.5f, 1f);

				GL11.glCallList(treeTronk);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

		// draw rock one
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(-1.4f, -0.4f, -5.2f);
			GL11.glRotated(45, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);
			GL11.glScalef(0.2f, 0.2f, 0.2f);
			GL11.glCallList(theRock);

			GL11.glPushMatrix();
			{
				GL11.glRotated(30, 0, 1, 0);
				GL11.glScalef(0.5f, 0.5f, 0.5f);
				GL11.glTranslatef(3f, 6f, 5f);
				GL11.glCallList(theRock);
			}
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();

		// draw rock two
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(-0.1f, -1.0f, -4.8f);
			GL11.glRotated(215, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);
			GL11.glScalef(0.2f, 0.1f, 0.1f);
			GL11.glCallList(theRock);
		}
		GL11.glPopMatrix();

		// draw rock three
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(1.3f, -0.7f, -4.6f);
			GL11.glRotated(-60, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);
			GL11.glScalef(0.15f, 0.15f, 0.15f);
			GL11.glCallList(theRock);

			GL11.glPushMatrix();
			{
				GL11.glRotated(30, 0, 1, 0);
				GL11.glScalef(0.5f, 0.5f, 0.5f);
				GL11.glTranslatef(3f, 6f, 5f);
				GL11.glCallList(theRock);
			}
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();

		// draw tree five
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(1.5f, 0.1f, -4.0f);
			GL11.glScalef(0.05f, 0.06f, 0.05f);
			GL11.glRotated(75, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);

			GL11.glCallList(treeTypeTwo);

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.3f, -8f, 0.2f);
				GL11.glScalef(0.8f, 1.0f, 0.8f);
				GL11.glCallList(treeTronk);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

		// draw tree six
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(0.4f, -0.5f, -3.5f);
			GL11.glScalef(0.02f, 0.02f, 0.02f);
			GL11.glRotated(75, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);
			GL11.glCallList(treeTypeTwo);
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.3f, -8f, 0.2f);
				GL11.glScalef(0.8f, 1.0f, 0.8f);
				GL11.glCallList(treeTronk);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

		// draw tree seven
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(-0.95f, -0.6f, -2.5f);
			GL11.glScalef(0.015f, 0.015f, 0.015f);
			GL11.glRotated(125, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);
			GL11.glCallList(treeTypeTwo);
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.3f, -8f, 0.2f);
				GL11.glScalef(0.8f, 1.0f, 0.8f);

				GL11.glCallList(treeTronk);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

		// draw grass one
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(-1.3f, -0.8f, -3f);
			GL11.glScalef(0.010f, 0.009f, 0.010f);
			GL11.glRotated(65, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);
			GL11.glCallList(grass);
			GL11.glPushMatrix();
			{
				GL11.glRotated(130, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glRotated(195, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glRotated(260, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

		// draw grass two
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(-0.75f, -0.8f, -3f);
			GL11.glScalef(0.010f, 0.009f, 0.010f);
			GL11.glRotated(65, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);
			GL11.glCallList(grass);

			GL11.glPushMatrix();
			{
				GL11.glRotated(130, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glRotated(195, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glRotated(260, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

		// draw grass three
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(0.55f, -0.8f, -3.40f);
			GL11.glScalef(0.010f, 0.009f, 0.010f);
			GL11.glRotated(65, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);
			GL11.glCallList(grass);

			GL11.glPushMatrix();
			{
				GL11.glRotated(130, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glRotated(195, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glRotated(260, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

		// draw grass four
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(1.90f, -0.8f, -4.0f);
			GL11.glScalef(0.015f, 0.015f, 0.015f);
			GL11.glRotated(65, 0, 1, 0);
			GL11.glRotatef(treeRotationAngle, 0.0f, -0.5f, 0.0f);
			GL11.glCallList(grass);
			GL11.glPushMatrix();
			{
				GL11.glRotated(130, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glRotated(195, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			{
				GL11.glRotated(260, 0, 1, 0);
				GL11.glCallList(grass);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();

	}

	/**
	 * Draws a plane aligned with the X and Z axis, with its front face toward
	 * positive Y. The plane is of unit width and height, and uses the current
	 * OpenGL material settings for its appearance
	 */
	private void drawUnitPlane() {
		Vertex v1 = new Vertex(-0.5f, 0.0f, -0.5f); // left, back
		Vertex v2 = new Vertex(0.5f, 0.0f, -0.5f); // right, back
		Vertex v3 = new Vertex(0.5f, 0.0f, 0.5f); // right, front
		Vertex v4 = new Vertex(-0.5f, 0.0f, 0.5f); // left, front

		// draw the plane geometry. order the vertices so that the plane faces up
		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(v4.toVector(), v3.toVector(), v2.toVector(), v1.toVector()).submit();

			GL11.glTexCoord2f(0.0f, 0.0f);
			v4.submit();

			GL11.glTexCoord2f(1.0f, 0.0f);
			v3.submit();

			GL11.glTexCoord2f(1.0f, 1.0f);
			v2.submit();

			GL11.glTexCoord2f(0.0f, 1.0f);
			v1.submit();
		}
		GL11.glEnd();

		// if the user is viewing an axis, then also draw this plane
		// using lines so that axis aligned planes can still be seen
		if (isViewingAxis()) {
			// also disable textures when drawing as lines
			// so that the lines can be seen more clearly
			GL11.glPushAttrib(GL11.GL_TEXTURE_2D);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glBegin(GL11.GL_LINE_LOOP);
			{
				v4.submit();
				v3.submit();
				v2.submit();
				v1.submit();
			}
			GL11.glEnd();
			GL11.glPopAttrib();
		}
	}
	/**
	 * The following methods draws tree base
	 * @param one
	 * @param two
	 * @param three
	 * @param four
	 * @param five
	 * @param six
	 * @param seven
	 * @param eight
	 */
	private void drawTreeBase(Colour one, Colour two, Colour three, Colour four, Colour five, Colour six, Colour seven,
			Colour eight) {
		Vertex v1 = new Vertex(0f, 0f, 5f);
		Vertex v2 = new Vertex(5f, 0f, 10f);
		Vertex v3 = new Vertex(10f, 0f, 10f);
		Vertex v4 = new Vertex(15f, 0f, 5f);
		Vertex v5 = new Vertex(10f, 0f, 0f);
		Vertex v6 = new Vertex(5f, 0f, 0f);

		Vertex v7 = new Vertex(2f, 3f, 5f);
		Vertex v8 = new Vertex(5f, 3f, 8f);
		Vertex v9 = new Vertex(10f, 3f, 8f);
		Vertex v10 = new Vertex(13f, 3f, 5f);
		Vertex v11 = new Vertex(10f, 3f, 2f);
		Vertex v12 = new Vertex(5f, 3f, 2f);

		// part one
		one.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v3.submit();
			v9.submit();
			v8.submit();
			v2.submit();
		}
		GL11.glEnd();
		// part two
		two.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v4.submit();
			v10.submit();
			v9.submit();
			v3.submit();
		}
		GL11.glEnd();
		// part three
		three.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v5.submit();
			v11.submit();
			v10.submit();
			v4.submit();
		}
		GL11.glEnd();
		// part four
		four.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v6.submit();
			v12.submit();
			v11.submit();
			v5.submit();
		}
		GL11.glEnd();
		// part five
		five.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v7.submit();
			v12.submit();
			v6.submit();
		}
		GL11.glEnd();
		// part six
		six.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v2.submit();
			v8.submit();
			v7.submit();
			v1.submit();
		}
		GL11.glEnd();
		// part seven
		seven.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v7.submit();
			v8.submit();
			v9.submit();
			v10.submit();
			v11.submit();
			v12.submit();
		}
		GL11.glEnd();
		// part eight
		eight.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v2.submit();
			v3.submit();
			v4.submit();
			v5.submit();
			v6.submit();
		}
		GL11.glEnd();

	}
	/**
	 * The following method draws tree top
	 * @param nine
	 * @param ten
	 * @param eleven
	 * @param twelve
	 * @param thirteen
	 * @param fourteen
	 * @param fifteen
	 */
	private void drawTreeTop(Colour nine, Colour ten, Colour eleven, Colour twelve, Colour thirteen, Colour fourteen,
			Colour fifteen) {

		Vertex v7 = new Vertex(2f, 3f, 5f);
		Vertex v8 = new Vertex(5f, 3f, 8f);
		Vertex v9 = new Vertex(10f, 3f, 8f);
		Vertex v10 = new Vertex(13f, 3f, 5f);
		Vertex v11 = new Vertex(10f, 3f, 2f);
		Vertex v12 = new Vertex(5f, 3f, 2f);
		Vertex v13 = new Vertex(7f, 7f, 5f);

		// part nine
		nine.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v8.submit();
			v13.submit();
			v7.submit();
		}
		GL11.glEnd();

		// part ten
		ten.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v9.submit();
			v13.submit();
			v8.submit();
		}
		GL11.glEnd();

		// part eleven
		eleven.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v10.submit();
			v13.submit();
			v9.submit();
		}
		GL11.glEnd();

		// part twelve
		twelve.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v11.submit();
			v13.submit();
			v10.submit();
		}
		GL11.glEnd();

		// part thirteen
		thirteen.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v12.submit();
			v13.submit();
			v11.submit();
		}
		GL11.glEnd();

		// part fourteen
		fourteen.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v7.submit();
			v13.submit();
			v12.submit();
		}
		GL11.glEnd();

		// part fifteen
		fifteen.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v7.submit();
			v8.submit();
			v9.submit();
			v10.submit();
			v11.submit();
			v12.submit();
		}
		GL11.glEnd();

	}
	/**
	 * The following methods draws tree's trunk
	 * @param partOne
	 * @param partTwo
	 * @param partThree
	 * @param partFour
	 * @param partFive
	 * @param partSix
	 */
	private void drawTreeTronk(Colour partOne, Colour partTwo, Colour partThree, Colour partFour, Colour partFive,
			Colour partSix) {

		Vertex v1 = new Vertex(7.5f, 0f, 3f);
		Vertex v2 = new Vertex(5.5f, 0f, 5f);
		Vertex v3 = new Vertex(7.5f, 0f, 7f);
		Vertex v4 = new Vertex(9.5f, 0f, 5f);

		Vertex v9 = new Vertex(7.5f, -6f, 3f);
		Vertex v10 = new Vertex(5.5f, -6f, 5f);
		Vertex v11 = new Vertex(7.5f, -6f, 7f);
		Vertex v12 = new Vertex(9.5f, -6f, 5f);

		// part one
		partOne.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v4.submit();
			v3.submit();
			v11.submit();
			v12.submit();
		}
		GL11.glEnd();
		// part two
		partTwo.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v3.submit();
			v2.submit();
			v10.submit();
			v11.submit();
		}
		GL11.glEnd();
		// part three
		partThree.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v2.submit();
			v1.submit();
			v9.submit();
			v10.submit();
		}
		GL11.glEnd();
		// part four
		partFour.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v4.submit();
			v12.submit();
			v9.submit();

		}
		GL11.glEnd();

		// part five
		partFive.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v2.submit();
			v3.submit();
			v4.submit();
		}
		GL11.glEnd();

		// part six
		partSix.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v9.submit();
			v10.submit();
			v11.submit();
			v12.submit();
		}
		GL11.glEnd();

	}

	/**
	 * The following methods draws the Stone [Truncated Octahedron]
	 * 
	 * @param partOne
	 * @param partTwo
	 * @param partThree
	 * @param partFour
	 * @param partFive
	 * @param partSix
	 * @param partSeven
	 * @param partEight
	 * @param partNine
	 * @param partTen
	 * @param parteEleven
	 * @param partTwelve
	 * @param partThirteen
	 * @param partFourteen
	 */
	public void drawTheRock(Colour partOne, Colour partTwo, Colour partThree, Colour partFour, Colour partFive,
			Colour partSix, Colour partSeven, Colour partEight, Colour partNine, Colour partTen, Colour parteEleven,
			Colour partTwelve, Colour partThirteen, Colour partFourteen) {
		Vertex v1 = new Vertex(0, 4, -2);
		Vertex v2 = new Vertex(-2, 4, 0);
		Vertex v3 = new Vertex(0, 4, 2);
		Vertex v4 = new Vertex(2, 4, 0);
		Vertex v5 = new Vertex(-4, 0, -2);
		Vertex v6 = new Vertex(-4, 2, 0);

		Vertex v7 = new Vertex(-4, 0, 2);
		Vertex v8 = new Vertex(-2, 0, 4);
		Vertex v9 = new Vertex(0f, 2f, 4f);
		Vertex v10 = new Vertex(2, 0, 4);
		Vertex v11 = new Vertex(4, 0, 2);
		Vertex v12 = new Vertex(4, 2, 0);

		Vertex v13 = new Vertex(4, 0, -2);
		Vertex v14 = new Vertex(2, 0, -4);
		Vertex v15 = new Vertex(0, 2, -4);
		Vertex v16 = new Vertex(2, 0, 4);
		Vertex v161 = new Vertex(-2, 0, -4);
		Vertex v17 = new Vertex(0, -4, -2);
		Vertex v18 = new Vertex(-2, -4, 0);

		Vertex v19 = new Vertex(0, -4, 2);
		Vertex v20 = new Vertex(2, -4, 0);
		Vertex v21 = new Vertex(-4, -2, 0);
		Vertex v22 = new Vertex(0, -2, 4);
		Vertex v23 = new Vertex(4, -2, 0);
		Vertex v24 = new Vertex(0, -2, -4);

		partOne.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v4.submit();
			v3.submit();
			v9.submit();
			v10.submit();
			v11.submit();
			v12.submit();
		}
		GL11.glEnd();

		partTwo.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v3.submit();
			v2.submit();
			v6.submit();
			v7.submit();
			v8.submit();
			v9.submit();
		}
		GL11.glEnd();

		partThree.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v2.submit();
			v1.submit();
			v15.submit();
			v161.submit();
			v5.submit();
			v6.submit();
		}
		GL11.glEnd();

		partFour.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v4.submit();
			v12.submit();
			v13.submit();
			v14.submit();
			v15.submit();
			v1.submit();
		}
		GL11.glEnd();

		partFive.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v8.submit();
			v7.submit();
			v21.submit();
			v18.submit();
			v19.submit();
			v22.submit();
		}
		GL11.glEnd();

		partSix.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v11.submit();
			v16.submit();
			v22.submit();
			v19.submit();
			v20.submit();
			v23.submit();

		}
		GL11.glEnd();

		partSeven.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v14.submit();
			v13.submit();
			v23.submit();
			v20.submit();
			v17.submit();
			v24.submit();
		}
		GL11.glEnd();

		partEight.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v5.submit();
			v161.submit();
			v24.submit();
			v17.submit();
			v18.submit();
			v21.submit();
		}
		GL11.glEnd();

		partNine.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v20.submit();
			v19.submit();
			v18.submit();
			v17.submit();
		}
		GL11.glEnd();

		partTen.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v2.submit();
			v3.submit();
			v4.submit();
		}
		GL11.glEnd();

		parteEleven.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v6.submit();
			v5.submit();
			v21.submit();
			v7.submit();
		}
		GL11.glEnd();

		partTwelve.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v8.submit();
			v22.submit();
			v10.submit();
			v9.submit();
		}
		GL11.glEnd();

		partThirteen.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v12.submit();
			v11.submit();
			v23.submit();
			v13.submit();
		}
		GL11.glEnd();

		partFourteen.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v161.submit();
			v15.submit();
			v14.submit();
			v24.submit();
		}
		GL11.glEnd();
	}
	/**
	 * The following method draw tree's type two
	 * @param partOne
	 * @param partTwo
	 * @param partThree
	 * @param partFour
	 * @param partFive
	 * @param partSix
	 * @param partsSeven
	 * @param partEight
	 */
	public void drawTreeTypeTwo(Colour partOne, Colour partTwo, Colour partThree, Colour partFour, Colour partFive,
			Colour partSix, Colour partsSeven, Colour partEight) {

		Vertex v1 = new Vertex(10, 8, 4);
		Vertex v2 = new Vertex(8, 8, 2);
		Vertex v3 = new Vertex(4, 8, 2);
		Vertex v4 = new Vertex(2, 8, 4);
		Vertex v5 = new Vertex(4, 8, 6);
		Vertex v6 = new Vertex(8, 8, 6);

		Vertex v7 = new Vertex(12, 0, 4);
		Vertex v8 = new Vertex(10, 0, 0);
		Vertex v9 = new Vertex(2, 0, 0);
		Vertex v10 = new Vertex(0, 0, 4);
		Vertex v11 = new Vertex(2, 0, 8);
		Vertex v12 = new Vertex(10, 0, 8);

		Vertex v13 = new Vertex(10, -8, 4);
		Vertex v14 = new Vertex(8, -8, 2);
		Vertex v15 = new Vertex(4, -8, 2);
		Vertex v16 = new Vertex(2, -8, 4);
		Vertex v17 = new Vertex(4, -8, 6);
		Vertex v18 = new Vertex(8, -8, 6);

		partOne.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v6.submit();
			v5.submit();
			v11.submit();
			v17.submit();
			v18.submit();
			v12.submit();
		}
		GL11.glEnd();

		partTwo.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v6.submit();
			v12.submit();
			v18.submit();
			v13.submit();
			v7.submit();
		}
		GL11.glEnd();

		partThree.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v2.submit();
			v1.submit();
			v7.submit();
			v13.submit();
			v14.submit();
			v8.submit();
		}
		GL11.glEnd();

		partFour.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v3.submit();
			v2.submit();
			v8.submit();
			v14.submit();
			v15.submit();
			v9.submit();
		}
		GL11.glEnd();

		partFive.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v4.submit();
			v3.submit();
			v9.submit();
			v15.submit();
			v16.submit();
			v10.submit();
		}
		GL11.glEnd();

		partSix.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v5.submit();
			v4.submit();
			v10.submit();
			v16.submit();
			v17.submit();
			v11.submit();
		}
		GL11.glEnd();

		partsSeven.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v2.submit();
			v3.submit();
			v4.submit();
			v5.submit();
			v6.submit();

		}
		GL11.glEnd();

		partEight.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v13.submit();
			v18.submit();
			v17.submit();
			v16.submit();
			v15.submit();
			v14.submit();
		}
		GL11.glEnd();
	}
	/**
	 * The following methods draws grass
	 * @param partOne
	 * @param partTwo
	 * @param partThree
	 * @param partFour
	 * @param partFive
	 */
	public void drawGrass(Colour partOne, Colour partTwo, Colour partThree, Colour partFour, Colour partFive) {

		Vertex v1 = new Vertex(6, 9, 3);
		Vertex v2 = new Vertex(3, 6, 2);
		Vertex v3 = new Vertex(2, 3, 2);
		Vertex v4 = new Vertex(2, 0, 2);
		Vertex v5 = new Vertex(4, 4, 2);
		Vertex v6 = new Vertex(4, 2, 2);

		Vertex v7 = new Vertex(4, 0, 2);
		Vertex v8 = new Vertex(3, 6, 4);
		Vertex v9 = new Vertex(2, 3, 4);
		Vertex v10 = new Vertex(2, 0, 4);
		Vertex v11 = new Vertex(4, 4, 4);
		Vertex v12 = new Vertex(4, 2, 4);

		Vertex v13 = new Vertex(4, 0, 4);

		partOne.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v8.submit();
			v9.submit();
			v10.submit();
			v13.submit();
			v12.submit();
			v11.submit();
		}
		GL11.glEnd();

		partTwo.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v11.submit();
			v12.submit();
			v13.submit();
			v7.submit();
			v6.submit();
			v5.submit();
		}
		GL11.glEnd();

		partThree.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v5.submit();
			v6.submit();
			v7.submit();
			v4.submit();
			v3.submit();
			v2.submit();
		}
		GL11.glEnd();

		partFour.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v1.submit();
			v2.submit();
			v3.submit();
			v4.submit();
			v10.submit();
			v9.submit();
			v8.submit();
		}
		GL11.glEnd();

		partFive.submit();
		GL11.glBegin(GL11.GL_POLYGON);
		{
			v13.submit();
			v4.submit();
			v10.submit();
			v13.submit();
		}
		GL11.glEnd();
	}

}
